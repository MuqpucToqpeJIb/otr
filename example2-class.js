class SomeCard extends React.Component {
    componentWillMount() {
        this.props.initPage(this.props.routeParams.id, undefined, VerificationApi.getRecord);
    }

    getActions() {
        return <div className="preview__actions">
            <div className="preview__actions-left">
                <div className="preview__action">
                    <Button text="Назад" color="blue" Icon={ArrLeftIcon} onClick={()=>{
                        hashHistory.goBack()
                    }} />
                </div>
            </div>
        </div>;
    }

    render() {
        const {record = {}, loading = false} = this.props;
        const {uvDemandPayments = {}} = record;

        return (
            <PreviewStandard actions={this.getActions()}>
                <TabComponent loading={loading} record={record}>
                    <VerificationBaseInfo key='Основные сведения' record={record} />
                    <VerificationProgressInfo key='Проверка' record={record} />
                    <VerificationPenalty key='Взыскание' uvDemandPayments={uvDemandPayments} />
                    <VerificationDocuments
                        key="Документы"
                        sectionPath={VerificationPaths.documentsRegistry}
                        getDocuments={VerificationApi.getDocumentsRegistry}
                        currentRecord={record}
                        checkId={record.id}
                    />
                </TabComponent>
            </PreviewStandard>
        );
    }
}

function mapStateToProps(state) {
    return {
        card: objectPath.get(state, "section.card"),
        loading: objectPath.get(state, "section.card.loading"),
        record: objectPath.get(state, "section.card.record")
    }
}

function mapDispatchToProps(dispatch) {
    return {
        initPage: (...args) => dispatch(initCardRequest(...args))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SomeCard)

