const Bitrix = lazy(() => import(/* webpackPrefetch: true */ 'components/Bitrix/Bitrix'));

function Some() {
  const dispatch = useDispatch();
  const { login, logout, send, openConnection } = useSocketConnection();
  const { showHonesty, showHelp, showLoginModal } = useSelector(state => state.modal);
  const isAuth = useSelector(getIsAuth);
  const user = useSelector(state => state.user);
  const bets = useSelector(state => state.bets);
  const msg = useSelector(state => state.msg);
  const lastPlayerData = useSelector(state => state.app.lastPlayerData);
  const prevlastPlayerData = usePrevious(lastPlayerData);
  const usersData = useSelector(state => state.app.usersData);
  const [odd, setOdd] = useState(0);
  const [isBetActive, setIsBetActive] = useState(false);
  const prevIsBetActive = usePrevious(isBetActive);
  const [isFirstLoad, setIsFirstLoad] = useState(true);
  const { apiUrl, betBlocks, sessionId } = useSelector(state => state.app);
  const appContainer = useRef(null);
  const roundId = useRef(0);

  const doubleBetMode = betBlocks.length === 2;

  const isMobile = useMediaQuery({ query: '(max-width: 640px)' });

  useEffect(() => {
    if (user.id === '' && lastPlayerData.userId) {
      dispatch(requestNextToken(lastPlayerData.userId));
    }
  }, [dispatch, lastPlayerData, user, login]);

  useEffect(() => {
    if (lastPlayerData && prevlastPlayerData.nextToken !== lastPlayerData.nextToken) {
      dispatch(
        setUser({
          id: lastPlayerData.userId
        })
      );
      login(lastPlayerData.userId, lastPlayerData.nextToken);
    }
  }, [dispatch, lastPlayerData, prevlastPlayerData, login]);

  const handleToggleModal = useCallback(
    (modalStateKey, show) => () => {
      toggleModalClass(show);
      dispatch(toggleModal(modalStateKey, show));
    },
    [dispatch]
  );

  const handleResetData = useCallback(() => {
    dispatch(resetData());
    window.history.replaceState({}, document.title, `${window.location.origin}${getLangQueryString()}`);
  }, [dispatch]);

  const handleBet = useCallback(
    (valueAmountBet, betBlockId, autoCashout = false, autoCashoutOdd) => {
      if (!isAuth) {
        console.debug('Необходимо авторизоваться');
        dispatch(showMsg('apolloPleaseAuthorize', 'error', betBlockId));
        return;
      }

      const isBetAlreadyMade = bets.some(bet => bet.betBlockId === betBlockId);

      if (isBetAlreadyMade) {
        console.debug('Ставка уже сделана');
        dispatch(showMsg('apolloBetIsAlreadyAccepted', 'error', betBlockId));
        return;
      }
      const { id } = user;
      const data = {
        roundId: roundId.current,
        sessionId: sessionId,
        amount: valueAmountBet,
        correlation: betBlockId,
        event: 'bet:place',
        ...(autoCashout && autoCashoutOdd ? { autoCashoutOdd } : {})
      };
      dispatch(addBet(data));
      sessionStorage.setItem('data from bet', JSON.stringify(data));
      dispatch(requestNextToken(id));
      console.log(bets);
      send(data);
    },
    [bets, dispatch, isAuth, roundId, send, sessionId]
  );

  const handleRequireAuth = useCallback(() => {
    const hasAuthorizeMsg = msg.some(msg => msg.text === 'apolloPleaseAuthorize');
    if (!isAuth && msg && !hasAuthorizeMsg) {
      // задержка исправляет «прыгающую» анимацию сообщений
      setTimeout(() => {
        dispatch(showMsg('apolloPleaseAuthorize', 'error'));
      }, 200);
    }
  }, [dispatch, msg, isAuth]);

  const betId = () => parseInt(sessionStorage.getItem('trans'), 10);

  const handleCashout = useCallback(
    betBlockId => {
      console.log('is cashout working?');
      const betData = JSON.parse(sessionStorage.getItem('data from bet'));
      const bet = bets.find(bet => bet.correlation === betData.betBlockId);
      if (!betId) {
        if (!bet || !bet.transactionId) {
          console.debug('Нет transactionId чтобы сделать кзшаут');
          dispatch(showMsg('apolloCashoutIsImpossible', 'error', betBlockId));
          return;
        }
      }
      const data = {
        roundId: betData.roundId,
        sessionId: sessionId,
        betId: betId(),
        correlation: betData.correlation,
        odd,
        event: 'bet:cashout'
      };
      sessionStorage.setItem('data from cashout', JSON.stringify(data));
      console.log('WHAT IS WRONG?', data);
      console.log('is cashout working?2');
      send(data);
    },
    [bets, dispatch, odd, send]
  );

  const handleFullscreen = () => toggleFullScreen(appContainer.current);

  const handleAddBetBlock = () => dispatch(addBetBlock());

  const handleRemoveBetBlock = betBlockId => dispatch(removeBetBlock(betBlockId));

  const handleLogout = useCallback(() => {
    logout();
    handleResetData();
  }, [handleResetData, logout]);

  const removeUserInactiveMessage = useCallback(() => {
    const userInactiveMessage = msg.find(msg => msg.text === 'apolloSessionExpired');
    if (userInactiveMessage) {
      dispatch(hideMsg(userInactiveMessage.id));
    }
  }, [dispatch, msg]);

  useEffect(() => {
    if (getCid() && getToken()) {
      dispatch(setUser({ id: getCid() }));
    }
    setPreloaderProgress(10);
  }, [dispatch]);

  useEffect(() => {
    if (Object.keys(lastPlayerData).length === 0) {
      dispatch(getlastData());
    }
  }, [dispatch, lastPlayerData]);

  useSocketEvent(socketEvents.message, () => {
    setPreloaderProgress(100);
    removeUserInactiveMessage();
    if (isFirstLoad) {
      appLoaded();
      setIsFirstLoad(false);
    }
    setTimeout(hidePreloader, 300);
  });

  useSocketEvent(
    socketEvents.gameAuthorized,
    useCallback(
      (event, data) => {
        console.debug(`[message] Пользователь авторизован: ${data}`);
        const { name, balance, currency, sessionId, status } = event;
        if (status !== 'ok') {
          handleLogout();
          switch (status) {
            case 'user_not_found':
              dispatch(showMsg('apolloAuthorizationUserNotFound', 'error'));
              break;
            case 'user_blocked':
              dispatch(showMsg('apolloAuthorizationUserBlocked', 'error'));
              break;
            case 'invalid_token' || 'unknown_error':
              dispatch(showMsg('apolloAuthorizationError', 'error'));
              break;
            default:
              break;
          }
          return;
        }
        const user = {
          name: name,
          balance: balance,
          currency: currency
        };
        dispatch(showMsg('apolloSuccessAuthorization', 'success', sessionId));
        dispatch(setUser(user));
        dispatch(setSessionId(sessionId));
        dispatch(requestListUserBets());
      },
      [dispatch, handleLogout]
    )
  );

  useSocketEvent(
    socketEvents.roundStarted,
    useCallback(event => {
      console.debug(`[message] Можно делать ставки`);
      if (roundId.current !== event.roundId) {
        roundId.current = event.roundId;
      }
    }, [])
  );

  useSocketEvent(
    socketEvents.gameExpired,
    useCallback(() => {
      dispatch(showMsg('apolloSessionExpired', 'error', '', true));
    }, [dispatch])
  );

  useSocketEvent(
    socketEvents.roundLaunched,
    useCallback(
      event => {
        console.debug(`[message] Раунд начался`);
        roundId.current = event.roundId;
        dispatch(roundLaunched());
      },
      [dispatch]
    )
  );

  useSocketEvent(
    socketEvents.roundRunning,
    useCallback(
      event => {
        // console.debug(`[message] Раунд идет. Коээфициент ${event.odd}`);
        roundId.current = event.roundId;
        setOdd(event.odd);
        dispatch(setBetAllowed(false));
      },
      [dispatch]
    )
  );

  useSocketEvent(
    socketEvents.roundCrashed,
    useCallback(
      event => {
        console.debug(`[message] Раунд окончен`);
        dispatch(setPrevRoundId(roundId.current));
        roundId.current = event.roundId;
        dispatch(setBetAllowed(false));
        dispatch(roundCrashed());
        dispatch(setShowPrevRound(true));
        setIsBetActive(false);
        setTimeout(() => {
          dispatch(roundStarted(roundId.current));
          dispatch(setShowPrevRound(false));
        }, 3000); // сбросить до начального состояния перед новой игрой
      },
      [dispatch]
    )
  );

  useSocketEvent(
    socketEvents.roundBetPlaced,
    useCallback(
      (event, data) => {
        console.debug(`[message] Кто-то сделал ставку: ${data}`);
        setIsBetActive(true);
        dispatch(setRoundBets(event));
      },
      [dispatch]
    )
  );

  useSocketEvent(
    socketEvents.roundBetCashouted,
    useCallback(
      (event, data) => {
        console.debug(`[message] Кто-то сделал кэшаут: ${data}`);
        setIsBetActive(false);
        dispatch(setRoundCashouts(event));
      },
      [dispatch]
    )
  );

  useSocketEvent(
    socketEvents.betPlaced,
    useCallback(
      (event, data) => {
        console.debug(`[message] Пользователь сделал ставку: ${data}`);
        const { balance, correlation, transactionId } = event;
        sessionStorage.setItem('trans', transactionId);
        if (event.status === 'SUCCESS') {
          dispatch(confirmBet(correlation, transactionId));
          dispatch(setUserBalance(balance));
          dispatch(showMsg('apolloBetAccepted', 'success', correlation));
          balanceChanged();
        } else {
          switch (event.status) {
            case 'BETS_LIMIT_EXCEEDED':
              dispatch(showMsg('apolloBetLimitExceeded', 'error', correlation));
              // для избежания дублирования ставки при автоставке с ошибкой
              dispatch(removeBet(correlation));
              break;
            case 'BET_IS_LATE':
              dispatch(showMsg('apolloBetIsLate', 'error', correlation));
              break;
            case 'USER_BLOCKED':
              dispatch(showMsg('apolloAuthorizationUserBlocked', 'error', correlation));
              break;
            case 'UNKNOWN':
              dispatch(showMsg('apolloGameUnavailable', 'error', correlation));
              break;
            case 'INSUFFICIENT_FUNDS':
              dispatch(showMsg('apolloInsufficientFunds', 'error', correlation));
              break;
            default:
              dispatch(showMsg('apolloBetAcceptError', 'error', correlation));
              break;
          }
        }
      },
      [dispatch]
    )
  );

  useSocketEvent(
    socketEvents.betSettled,
    useCallback(
      (event, data) => {
        console.debug(`[message] Пользователь сделал кэшаут: ${data}`);
        const { balance, correlation } = event;
        if (event.status === 'SUCCESS') {
          dispatch(showMsg('apolloCashoutComplete', 'success', correlation));
          dispatch(removeBet(correlation));
          dispatch(setUserBalance(balance));
          balanceChanged();
        } else {
          dispatch(removeBet(correlation));
          dispatch(showMsg('apolloCashoutError', 'error', correlation));
        }
      },
      [dispatch]
    )
  );

  useSocketEvent(
    socketEvents.close,
    useCallback(() => {
      dispatch(resetData());
    }, [dispatch])
  );

  useEffect(() => {
    if (apiUrl) {
      dispatch(requestCurrencies());
      dispatch(requestListBetHistory());
      dispatch(requestListHighestBets());
    }
  }, [apiUrl, dispatch]);

  useEffect(() => {
    removeConsolesFromProduction();
  }, []);

  const handleSessionContinue = useCallback(() => {
    openConnection();
  }, [openConnection]);

  const userLogin = useCallback(
    (id, token) => {
      if (!id || !token) {
        return;
      }
      dispatch(
        setUser({
          id: id
        })
      );
      dispatch(setUserCid(id, token));
      dispatch(requestNextToken(id));
      login(id, token);
    },
    [dispatch, login]
  );

  const isUserAway = msg.some(msg => msg.text === 'apolloSessionExpired');

  return (
    <div className="wrapper">
      {isPromo() && (
        <Suspense fallback={<Loader />}>
          <Bitrix />
        </Suspense>
      )}
      <div className={`app app--${getLang()} ${isFullscreen() ? 'app--fullscreen' : ''}`} ref={appContainer}>
        <AppVersion />
        {isUserAway && <div className="app__away-overlay" onClick={handleSessionContinue} />}
        <div className="app__wrapper">
          <Header
            user={user}
            onShowHonesty={handleToggleModal('showHonesty', true)}
            onShowHelp={handleToggleModal('showHelp', true)}
            onShowLoginModal={handleToggleModal('showLoginModal', true)}
            onLogout={handleLogout}
            onFullscreen={handleFullscreen}
            onMobileMenuOpen={handleToggleModal('showMobileMenu', true)}
            isAuth={isAuth}
          />

          <div className="main">
            <div className="main__block main__block--left">
              <TableBetsRoundAndUser />
            </div>

            <div className="main__block main__block--center">
              <Game pathToGame={getPathToGameWithLocale()} />
              <div className="main__block--bets">
                {betBlocks.map(betBlockId => (
                  <Bet
                    key={betBlockId}
                    onBet={handleBet}
                    onCashout={handleCashout}
                    odd={odd}
                    roundId={roundId}
                    id={betBlockId}
                    squashed={doubleBetMode}
                    requireAuth={handleRequireAuth}
                    onRemoveBet={handleRemoveBetBlock}
                  />
                ))}
                {!doubleBetMode && <AddBetBlock onAddBet={handleAddBetBlock} />}
              </div>
            </div>

            <div className="main__block main__block--right">
              <History />
            </div>
          </div>
        </div>
        {showHonesty && (
          <HonestyModal
            onClose={handleToggleModal('showHonesty', false)}
            onShowMobileMenu={handleToggleModal('showMobileMenu', true)}
          />
        )}
        {showHelp && (
          <HowItWorksModal
            onClose={handleToggleModal('showHelp', false)}
            onShowMobileMenu={handleToggleModal('showMobileMenu', true)}
          />
        )}
        {showLoginModal && (
          <AuthModal onLogin={userLogin} onClose={handleToggleModal('showLoginModal', false)} />
        )}
        {isMobile && <MobileApp toggleModal={handleToggleModal} handleLogout={handleLogout} />}
      </div>
    </div>
  );
}

export default Some;
